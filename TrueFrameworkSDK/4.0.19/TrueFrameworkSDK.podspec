#
# Be sure to run `pod lib lint TrueFrameworkSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'TrueFrameworkSDK'
s.version          = '4.0.19'
s.summary          = 'True SDK Login for swift 4.0'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
TODO: Add long description of the pod here.
for 4.0.8 update refresh token scope case trueid version 3
version 4.0.9
-- change set client id and secret key for security
-- dismiss progress bar sometimes for show progress
-- remove some print errors
DESC

s.homepage         = 'https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'swordmanboy' => 'apinun.wong@gmail.com' }
s.source           = { :git => 'git@bitbucket.org:truedmp/trueid-sdk-4.0-ios-demo.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '8.3'


s.source_files = 'TrueFrameworkSDK/Classes/**/*','Classes/*.{h,m}'
#,'TrueFrameworkSDK/Framework/TrueIDFramework.framework/Headers/*.h'
#s.public_header_files = 'TrueFrameworkSDK/Framework/TrueIDFramework.framework/Headers/*.h'

#s.ios.public_header_files  = 'TrueFrameworkSDK/Framework/TrueIDFramework.framework/Headers/*.h'
#s.frameworks = 'UIKit' #'TrueIDFramework' #'MapKit'           #fixed for framework.h not found

s.ios.vendored_frameworks = "TrueFrameworkSDK/Framework/TrueIDFramework.framework"

end

