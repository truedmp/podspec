#
# Be sure to run `pod lib lint TrueFrameworkSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#


Pod::Spec.new do |s|
s.name             = 'TrueFrameworkSDK'
s.version          = '4.3.11'
s.summary          = 'True SDK Login for swift 4.2'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
TODO: Add long description of the pod here.
version 4.0.32
-- Added
    - Print log when application using SDKV.4 that operating.
-- Fixed Bug
    - Application is log in after Application using SDK V.3 that
      update to SDK v.4
    - TrueId Application is loged in after Application using SDK V.3 that
      update to SDK v.4
    - Applicatin hide progress bar after forget password success for Ipad Device.=
DESC

s.homepage         = 'https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'Apinun Wongintawang' => 'apinun.wong@gmail.com' }
s.source           = { :git => 'git@bitbucket.org:truedmp/trueid-sdk-4.0-ios-demo.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '8.3'


#s.source_files = 'TrueFrameworkSDK/Classes/**/*','Classes/*.{h,m}'
#,'TrueFrameworkSDK/Framework/TrueIDFramework.framework/Headers/*.h'
#s.public_header_files = 'TrueFrameworkSDK/Framework/TrueIDFramework.framework/Headers/*.h'

#s.ios.public_header_files  = 'TrueFrameworkSDK/Framework/TrueIDFramework.framework/Headers/*.h'
#s.frameworks = 'UIKit' #'TrueIDFramework' #'MapKit'           #fixed for framework.h not found

s.ios.vendored_frameworks = "TrueFrameworkSDK/Framework/TrueIDFramework.framework"

end

