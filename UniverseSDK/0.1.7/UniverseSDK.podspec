#
# Be sure to run `pod lib lint UniverseSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UniverseSDK'
  s.version          = '0.1.7'
  s.summary          = 'A packed UniverseSDK for iOS.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  Pod for UniverSDK. Allow user to jumpback to True ID easily.

  The SDK supports both Swift and Objective-C language.
                       DESC

  s.homepage         = 'https://bitbucket.org/truedmp/universesdk-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Kittiphat S.' => 'freeuxer@gmail.com' }
  s.source           = { :git => 'git@bitbucket.org:truedmp/universesdk-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.3'

  s.source_files = 'UniverseSDK/Classes/**/*'
  
  s.resource_bundles = {
    'UniverseSDK' => ['UniverseSDK/Assets/*']
  }

  s.pod_target_xcconfig = {
    'FRAMEWORK_SEARCH_PATHS' => '$(inherited) $(PODS_ROOT)/**'
  }

  s.ios.frameworks = 'UIKit', 'AVFoundation'

  s.dependency 'Alamofire', '~> 4.4.0'
  s.dependency 'AlamofireObjectMapper', '~> 4.1.0'
  s.dependency 'SnapKit', '~> 3.2.0'
  s.dependency 'CryptoSwift', '~> 0.6.9'
  s.dependency 'PromiseKit', '~> 4.2.0'
  s.dependency 'SwiftyRSA', '~> 1.2.0'
end
