#
# Be sure to run `pod lib lint TrueIDProfileSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TrueIDProfileSDK'
  s.version          = '0.1.0'
  s.summary          = 'A short description of TrueIDProfileSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/truedmp/profilesdk-ios-demo'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Tanet Pornsirirat' => 'tanet.por@truedigital.com' }
  s.source           = { :git => 'git@bitbucket.org:truedmp/profilesdk-ios-demo.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'
  s.ios.vendored_frameworks = "TrueIDProfileSDK/Framework/TrueIDProfileSDK.framework"
  # s.source_files = 'TrueIDProfileSDK/Classes/**/*'
  
  # s.resource_bundles = {
  #   'TrueIDProfileSDK' => ['TrueIDProfileSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
