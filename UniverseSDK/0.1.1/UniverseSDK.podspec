#
# Be sure to run `pod lib lint UniverseSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UniverseSDK'
  s.version          = '0.1.1'
  s.summary          = 'A packed UniverseSDK for iOS.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  Pod for UniverSDK. Allow user to jumpback to True ID easily.

  The SDK supports both Swift and Objective-C language.
                       DESC

  s.homepage         = 'https://bitbucket.org/truedmp/universesdk-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Kittiphat S.' => 'freeuxer@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/truedmp/universesdk-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.3'

  s.source_files = 'UniverseSDK/Classes/**/*'
  
  s.resource_bundles = {
    'UniverseSDK' => ['UniverseSDK/Assets/*']
  }

  s.pod_target_xcconfig = {
    'FRAMEWORK_SEARCH_PATHS' => '$(inherited) $(PODS_ROOT)/**',
#    'USER_HEADER_SEARCH_PATHS' => '$(PODS_ROOT)/FirebaseCore.framework/Modules',
#    'SWIFT_INCLUDE_PATHS' => '$(PODS_ROOT)/FirebaseCore.framework/Modules',
#    'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES',
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.ios.frameworks = 'UIKit', 'AVFoundation'

  s.dependency 'AlamofireObjectMapper'
  s.dependency 'SnapKit'
  s.dependency 'CryptoSwift'
  s.dependency 'PromiseKit', '~> 4.0'
  s.dependency 'SwiftyRSA'
  s.dependency 'RSKImageCropper'
end
